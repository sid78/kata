package com.example.kata.mapper;

import com.example.kata.dto.ClientDto;
import com.example.kata.entities.Client;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ClientMapper {
    ClientDto toDto(Client client);
    Client toClientEntity(ClientDto dto);
}
