package com.example.kata.mapper;

import com.example.kata.dto.CreneauDto;
import com.example.kata.entities.Creneau;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CreneauMapper {
    CreneauDto toDto(Creneau creneau);
    Creneau toCreneauEntity(CreneauDto dto);
}