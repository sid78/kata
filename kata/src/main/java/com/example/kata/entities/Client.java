package com.example.kata.entities;

import com.example.kata.enumeration.ModeLivraison;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private ModeLivraison modeLivraisonChoisi;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
    private List<Creneau> creneaux = new ArrayList<>();

    public void choisirModeLivraison(ModeLivraison modeLivraison) {
        this.modeLivraisonChoisi = modeLivraison;
    }
}
