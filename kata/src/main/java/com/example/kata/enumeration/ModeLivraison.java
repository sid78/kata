package com.example.kata.enumeration;

public enum ModeLivraison {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP
}
