package com.example.kata.dto;

import com.example.kata.entities.Creneau;
import com.example.kata.enumeration.ModeLivraison;
import lombok.Data;

@Data
public class ClientDto {
    private ModeLivraison modeLivraisonChoisi;
    private Creneau creneauChoisi;
}
