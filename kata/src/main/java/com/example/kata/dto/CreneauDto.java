package com.example.kata.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreneauDto {

    private DayOfWeek jour;
    private LocalTime heureDebut;
    private LocalTime heureFin;
}
