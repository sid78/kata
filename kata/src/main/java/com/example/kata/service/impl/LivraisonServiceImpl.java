package com.example.kata.service.impl;

import com.example.kata.dto.CreneauDto;
import com.example.kata.entities.Client;
import com.example.kata.entities.Creneau;
import com.example.kata.enumeration.ModeLivraison;
import com.example.kata.mapper.CreneauMapper;
import com.example.kata.repository.ClientRepository;
import com.example.kata.repository.CreneauRepository;
import com.example.kata.service.LivraisonService;
import org.springframework.stereotype.Service;

@Service
public class LivraisonServiceImpl implements LivraisonService {
    private final ClientRepository clientRepository;
    private final CreneauRepository creneauRepository;
    private final CreneauMapper creneauMapper;

    public LivraisonServiceImpl(ClientRepository clientRepository, CreneauRepository creneauRepository, CreneauMapper CreneauMapper) {
        this.clientRepository = clientRepository;
        this.creneauRepository = creneauRepository;
        this.creneauMapper = CreneauMapper;
    }

    @Override
    public void choisirModeLivraison(Long clientId, ModeLivraison modeLivraison) {
        Client client = clientRepository.findById(clientId)
                .orElseThrow(() -> new RuntimeException("Client not found"));
        client.choisirModeLivraison(modeLivraison);
        clientRepository.save(client);
    }

    @Override
    public void choisirCreneau(Long clientId, CreneauDto creneauDto) {
        Client client = clientRepository.findById(clientId)
                .orElseThrow(() -> new RuntimeException("Client not found"));
        Creneau creneau = creneauMapper.toCreneauEntity(creneauDto);
        creneau.setClient(client);
        client.getCreneaux().add(creneau);
        clientRepository.save(client);
    }
}
