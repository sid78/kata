package com.example.kata.service;

import com.example.kata.dto.CreneauDto;
import com.example.kata.enumeration.ModeLivraison;

public interface LivraisonService {
    void choisirModeLivraison(Long clientId, ModeLivraison modeLivraison);
    void choisirCreneau(Long clientId, CreneauDto creneauDto);
}
