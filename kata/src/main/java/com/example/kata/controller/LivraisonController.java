package com.example.kata.controller;


import com.example.kata.dto.CreneauDto;
import com.example.kata.enumeration.ModeLivraison;
import com.example.kata.service.LivraisonService;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping("/livraison")
@SecurityScheme(
        name="Bearer Authentication",
        type= SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)
@SecurityRequirement(name="Bearer Authentication")
public class LivraisonController {
    private final LivraisonService livraisonService;

    public LivraisonController(LivraisonService livraisonService) {
        this.livraisonService = livraisonService;
    }

    @PostMapping("/choisir-mode")
    public ResponseEntity<EntityModel<String>> choisirModeLivraison(@RequestParam Long clientId, @RequestParam ModeLivraison modeLivraison) {
        livraisonService.choisirModeLivraison(clientId, modeLivraison);
        EntityModel<String> response = EntityModel.of("Mode de livraison choisi : " + modeLivraison);
        Link creneauLink = linkTo(methodOn(LivraisonController.class).choisirCreneau(clientId, null)).withRel("choisir-creneau");
        response.add(creneauLink);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/choisir-creneau")
    public ResponseEntity<String> choisirCreneau(@RequestParam Long clientId, @RequestBody CreneauDto creneauDto) {
        livraisonService.choisirCreneau(clientId, creneauDto);
        return ResponseEntity.ok("Créneau choisi : " + creneauDto.getJour() + " " + creneauDto.getHeureDebut() + "-" + creneauDto.getHeureFin());
    }
}
