package com.example.kata.controller;

import com.example.kata.dto.CreneauDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KafkaProducerController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @PostMapping("/message")
    public void sendMessage(@RequestBody CreneauDto creneauDto) {
        kafkaTemplate.send("topic", creneauDto.toString());
    }
}
