package com.example.kata;

import com.example.kata.dto.CreneauDto;
import com.example.kata.enumeration.ModeLivraison;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;


import java.time.DayOfWeek;
import java.time.LocalTime;

import static io.restassured.RestAssured.given;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LivraisonControllerTest {

    @LocalServerPort
    private int port;

    @Test
    public void testChoisirModeLivraison() {
        RestAssured.baseURI = "http://localhost:" + port;

        given()
                .contentType(ContentType.JSON)
                .param("clientId", 1)
                .param("modeLivraison", ModeLivraison.DRIVE)
                .when()
                .post("/livraison/choisir-mode")
                .then()
                .statusCode(200);
    }

    @Test
    public void testChoisirCreneau() {
        RestAssured.baseURI = "http://localhost:" + port;

        CreneauDto creneauDto = new CreneauDto();
        creneauDto.setJour(DayOfWeek.MONDAY); // Par exemple, pour un lundi
        creneauDto.setHeureDebut(LocalTime.of(9, 0)); // 09:00
        creneauDto.setHeureFin(LocalTime.of(10, 0)); // 10:00

        given()
                .contentType(ContentType.JSON)
                .param("clientId", 1)
                .body(creneauDto)
                .when()
                .post("/livraison/choisir-creneau")
                .then()
                .statusCode(200);
    }
}
